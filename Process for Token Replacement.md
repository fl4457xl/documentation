# Process for token replacement

Hardware tokens are provided for employees and students who do not have a phone to facilitate multi-factor authentication in Office 365.

## Staff
* The first token is provided at no cost.
* Subsequent tokens are provided for purchase, or a different MFA method can be utilized instead.
* Funds will be transferred from the departmental cost center.


---

## Students
* The first token is provided at no cost.
* Subsequent tokens are provided for purchase, or a different MFA method can be utilized instead.
* Additional tokens can be purchased from the <where will we be able to put these? Bills & Payments?>