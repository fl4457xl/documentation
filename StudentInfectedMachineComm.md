Hello,

Our records show that you logged into our wireless network on <insert date/time here>.  At this time, traffic was detected from your device that indicate that your device might have malware (malicious software).  The device in question is a <insert device information here>.

We would recommend that you run anti-virus software on your computer.  Windows has a pretty good built-in scanner through the Windows Security Center.  Otherwise, a good 3rd party anti-virus scanner is Malwarebytes.

Please note that we do not endorse or support software we recommend, we just comment that it is reliable.

If you need further assistance, Microcenter in St. Louis Park or Best Buy provide services 